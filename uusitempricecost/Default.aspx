﻿<%@ Page ValidateRequest="true" EnableEventValidation="true" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="uusitempricecost.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Alert jquery style start --%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="Scripts/jquery-3.4.1.js"></script>
    <script src="Scripts/jquery-3.4.1.min.map.js"></script>
    <script type="text/javascript" src="MaxLength.min.js"></script>
    <link href="Scripts/jqueryconfirm/jquery-confirm.min.css" rel="stylesheet" />
    <script src="Scripts/jqueryconfirm/jquery-confirm.min.js"></script>
    <style type="text/css">
        .jconfirm .jconfirm-box {
            background: white;
            border-radius: 10px;
            position: relative;
            outline: 5px;
        }
    </style>
    <script type="text/javascript">
        //Validate for numbers only 
        $(function (evt) {
            debugger;
            // Validate for numbers only in Search Vendor Textbox
            $("input[type=text]").keypress(function () {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                if (charCode == 13) {
                    document.getElementById("MainContent_btnFind").click();
                    return false;
                }
                return true;
            });
        });
        function fnCatReport() {
            $.alert("Future Functionality?")
        }

    </script>
    <%-- Alert jquery style end --%>
    <div class="container min-container">
        <%-- Declare a design template --%>
        <link href="/Content/Site1.css" rel="stylesheet" />

        <div class="page-title">Vendor Categories</div>
        <asp:Panel runat="server" ID="pnlVendor">
            <div class="vendordisptable">
                <div id="popup" style="max-height: 600px; overflow-y: scroll;">
                    <%-- Gridview for vendor Display screen --%>
                    <asp:GridView runat="server" ID="grdVendorDisplay" AutoGenerateColumns="false" OnPageIndexChanging="grdVendorDisplay_PageIndexChanging" OnRowCommand="grdVendorDisplay_RowCommand"
                        Width="100%" HeaderStyle-BackColor="Yellow" AllowSorting="true" CellPadding="4">
                        <Columns>
                            <asp:BoundField DataField="VendorName" HeaderText="Vendor Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-Height="30px" ItemStyle-Width="30%" />
                            <asp:TemplateField HeaderText="Vendor Number" ItemStyle-Width="10%">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_VendorNumber" runat="server" Text='<%# Eval("VendorNumber") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="10%">
                                <HeaderTemplate>
                                    <asp:Label ID="lbl_Defined" runat="server" Text="Defined Categories" ToolTip="Defined Categories"></asp:Label>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Define" runat="server" Text='<%# Bind("DefinedCategories") %>' ToolTip="Defined Categories" CssClass="lblDefined"></asp:Label>
                                    <asp:ImageButton runat="server" Width="20" Height="20" ImageAlign="Right" ID="btn" ImageUrl="~/Images/arrow.png"
                                        CommandName="Select" CommandArgument="<%# Container.DataItemIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="50%" DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200px" />
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
                    </asp:GridView>
                </div>
            </div>

            <%--  Find,Reset,Search,Show all buttons --%>
            <div id="divBtn" class="editcontent">
                <asp:Panel runat="server" ID="editinputcontent">
                    <div class="row">
                        <div class="col-md-5">
                            <asp:Label runat="server" ID="lblLastValueEnter" Text="Last Value Entered:" CssClass="lblValueEnter"></asp:Label>
                            <asp:Label runat="server" ID="lblLastValue" CssClass="lblValueEnter"></asp:Label>
                        </div>
                    </div>
                    <div class="row vendorfooter">
                        <div class="col-md-5">
                            <asp:Button runat="server" ID="btnVendorCat_Report" Text="Vendor Cat Report" OnClick="btnVendorCat_Report_Click" />
                            <asp:Button runat="server" ID="btnShow_All" Text="Show All Categories" OnClick="btnShow_All_Click" />
                        </div>
                        <div class="col-md-7">
                            <asp:Label runat="server" ID="lblVendor" Text="Vendor No.:" CssClass="lblVendor"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_SearchVendor" Height="40px" MaxLength="19">
                            </asp:TextBox>
                            <asp:Button runat="server" Text="Find It" ID="btnFind" Height="40px" OnClick="btnFind_Click" />
                            <asp:Button runat="server" Text="Reset" ID="btnReset" Height="40px" OnClick="btnReset_Click" />
                        </div>
                        <div class="col-md-7">
                            <asp:Label runat="server" ID="lblErrorMessage"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        </asp:Panel>
    </div>


</asp:Content>

