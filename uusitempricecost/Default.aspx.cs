﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace uusitempricecost
{
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page load - Add global data here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Declare the Initialize value,Method name here
            

        }

        protected void Page_PreRender(object o, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // Declaring the gridview bind event
                grdVendorDisplay.DataSource = BindDisplayGrid();
                grdVendorDisplay.DataBind();

            }
            if (Session["Search"] != null && Session["Search"].ToString() != "")
            {
                lblLastValue.Text = Session["Search"].ToString();
            }
            if (Session["Search"] != null && Session["Search"].ToString() != "")
            {
                FindAfterUpdate(Session["Search"].ToString());
            }
            if (Session["btn"] != null && Session["btn"].ToString() != "")
            {
                Show_All_Click(Session["btn"].ToString());
            }
        }
        #region Vendor Class 
        public class VendorList
        {
            public string VendorName { get; set; }
            public string VendorNumber { get; set; }
            public string DefinedCategories { get; set; }
            public string Description { get; set; }
        }
        #endregion

        #region Gridview events 
        /// <summary>
        /// Gridview binding  in display screen
        /// </summary>
        private List<VendorList> BindDisplayGrid()
        {
            List<VendorList> Results = new List<VendorList>();
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_vendorcategory", commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorList>().ToList();

                grdVendorDisplay.DataSource = Results;
                grdVendorDisplay.DataBind();
            }
            return Results;
        }

        public List<VendorList> ShowAllCategoriesBind()
        {
            List<VendorList> Results = new List<VendorList>();
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                VendorList resr = new VendorList();
                var mpresults = db.QueryMultiple("sp_get_undefining_vendorcategory", commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorList>().ToList();
                grdVendorDisplay.DataSource = Results;
                grdVendorDisplay.DataBind();
            }
            return Results;
        }
        protected void grdVendorDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdVendorDisplay.PageIndex = e.NewPageIndex;


            grdVendorDisplay.SelectedIndex = -1;
            if (btnShow_All.Text == "Show All Categories")
            {
                grdVendorDisplay.DataSource = Session["objects"];
            }
            else
            {
                this.ShowAllCategoriesBind();

            }
        }



        //grid view row command for to redirect the update screen
        protected void grdVendorDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //Determine the RowIndex of the Row whose Button was clicked.
                int rowIndex = Convert.ToInt32(e.CommandArgument);

                //Reference the GridView Row.
                GridViewRow row = grdVendorDisplay.Rows[rowIndex];

                //Fetch value of VendorNumber.
                string VendorNumber = (row.FindControl("lbl_VendorNumber") as Label).Text;

                //Fetch value of VendorCategoryNo.
                string VendorCategoryNo = (row.FindControl("lbl_Define") as Label).Text;

                Response.Redirect("UpdateVendorCategory.aspx?VendorNo=" + VendorNumber + "&VendorCategory=" + VendorCategoryNo + "");
            }
        }
        #endregion

        protected void btnVendorCat_Report_Click(object sender, EventArgs e)
        {
            string jsFunc = "fnCatReport()";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);

        }
        #region Show All button click when change the grid label and items
        protected void btnShow_All_Click(object sender, EventArgs e)
        {
            Session["btn"] = "";
            if (grdVendorDisplay.HeaderRow != null)
            {
                Label txt = (Label)grdVendorDisplay.HeaderRow.FindControl("lbl_Defined");

                List<VendorList> Results = new List<VendorList>();

                if (btnShow_All.Text == "Show All Categories")
                {
                    if (Session["Search"] != null && Session["Search"].ToString() != "")
                    {
                        var param = new DynamicParameters();
                        param.Add("@SearchVendor", Session["Search"].ToString().Trim());
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_filter_undefining_vendorcategory", param, commandType: CommandType.StoredProcedure);
                            Results = mpresults.Read<VendorList>().ToList();
                            grdVendorDisplay.CssClass = "grdvendorscroll";
                            grdVendorDisplay.DataSource = Results;
                            grdVendorDisplay.DataBind();
                        }
                        btnShow_All.Text = "Undefined Categories";
                        ((Label)grdVendorDisplay.HeaderRow.FindControl("lbl_Defined")).Text = "All Categories";
                    }
                    else
                    {
                        Results = ShowAllCategoriesBind();

                        //bind the items in gridview
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                        btnShow_All.Text = "Undefined Categories";
                        ((Label)grdVendorDisplay.HeaderRow.FindControl("lbl_Defined")).Text = "All Categories";
                    }

                }
                else
                 {
                    if (Session["Search"] != null && Session["Search"].ToString() != "")
                    {
                        var param = new DynamicParameters();
                        param.Add("@SearchVendor", Session["Search"].ToString().Trim());
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_filter_vendorcategory", param, commandType: CommandType.StoredProcedure);
                            Results = mpresults.Read<VendorList>().ToList();
                            grdVendorDisplay.CssClass = "grdvendorscroll";
                            grdVendorDisplay.DataSource = Results;
                            grdVendorDisplay.DataBind();
                        }
                        btnShow_All.Text = "Show All Categories";
                        ((Label)grdVendorDisplay.HeaderRow.FindControl("lbl_Defined")).Text = "Defined Categories";
                    }
                    else
                    {
                        Results = BindDisplayGrid();
                        //bind the items in gridview
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                        btnShow_All.Text = "Show All Categories";
                        ((Label)grdVendorDisplay.HeaderRow.FindControl("lbl_Defined")).Text = "Defined Categories";
                    }
                }
            }
            Session["btn"] = btnShow_All.Text;
        }

        #endregion


        #region Find,Reset click event
        protected void btnFind_Click(object sender, EventArgs e)
        {
            // Session["Search"] = "";
            List<VendorList> Results = new List<VendorList>();
            if (Session["btn"] !=null)
            {
                if (Session["btn"].ToString() == "Show All Categories")
                {
                    if (!string.IsNullOrEmpty(txt_SearchVendor.Text.Trim()))
                    {
                        var param = new DynamicParameters();
                        param.Add("@SearchVendor", txt_SearchVendor.Text.Trim());

                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_filter_vendorcategory", param, commandType: CommandType.StoredProcedure);
                            Results = mpresults.Read<VendorList>().ToList();
                            grdVendorDisplay.CssClass = "grdvendorscroll";
                            grdVendorDisplay.DataSource = Results;
                            grdVendorDisplay.DataBind();
                            //Before clearing the text box to maintain session
                            Session["Search"] = txt_SearchVendor.Text;
                            //After find the details text box cleared
                            txt_SearchVendor.Text = string.Empty;
                            txt_SearchVendor.Focus();
                            if (Session["Search"] != null && Session["Search"].ToString() != "")
                            {
                                lblLastValue.Text = Session["Search"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    var param = new DynamicParameters();
                    param.Add("@SearchVendor", txt_SearchVendor.Text.ToString().Trim());
                    using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                    {
                        var mpresults = db.QueryMultiple("sp_get_filter_undefining_vendorcategory", param, commandType: CommandType.StoredProcedure);
                        Results = mpresults.Read<VendorList>().ToList();
                        grdVendorDisplay.CssClass = "grdvendorscroll";
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                        Session["Search"] = txt_SearchVendor.Text;
                        //After find the details text box cleared
                        txt_SearchVendor.Text = string.Empty;
                        txt_SearchVendor.Focus();
                        if (Session["Search"] != null && Session["Search"].ToString() != "")
                        {
                            lblLastValue.Text = Session["Search"].ToString();
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txt_SearchVendor.Text.Trim()))
                {
                    var param = new DynamicParameters();
                    param.Add("@SearchVendor", txt_SearchVendor.Text.Trim());

                    using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                    {
                        var mpresults = db.QueryMultiple("sp_get_filter_vendorcategory", param, commandType: CommandType.StoredProcedure);
                        Results = mpresults.Read<VendorList>().ToList();
                        grdVendorDisplay.CssClass = "grdvendorscroll";
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                        //Before clearing the text box to maintain session
                        Session["Search"] = txt_SearchVendor.Text;
                        //After find the details text box cleared
                        txt_SearchVendor.Text = string.Empty;
                        txt_SearchVendor.Focus();
                        if (Session["Search"] != null && Session["Search"].ToString() != "")
                        {
                            lblLastValue.Text = Session["Search"].ToString();
                        }
                    }
                }
            }
            
                if (Results.Count == 0)
                {
                    lblErrorMessage.CssClass = "lblError";
                    lblErrorMessage.Text = "Records not found.Please enter correct vendor number";
                }
                else
                {
                    lblErrorMessage.Text = "";
                }

            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txt_SearchVendor.Text = string.Empty;
            lblErrorMessage.Text = string.Empty;
            Session["Search"] = string.Empty;
            lblLastValue.Text = string.Empty;

            txt_SearchVendor.Focus();
            this.BindDisplayGrid();
        }
        #endregion

        
        public void FindAfterUpdate(string search)
        {
            if (search != null && search.ToString() != "")
            {
                List<VendorList> Results = new List<VendorList>();
                var param = new DynamicParameters();
                param.Add("@SearchVendor", search);


                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                {
                    var mpresults = db.QueryMultiple("sp_get_filter_vendorcategory", param, commandType: CommandType.StoredProcedure);
                    Results = mpresults.Read<VendorList>().ToList();
                    grdVendorDisplay.CssClass = "grdvendorscroll";
                    grdVendorDisplay.DataSource = Results;
                    grdVendorDisplay.DataBind();
                }
            }
        }


        public void Show_All_Click(string btnShow)
            {
                List<VendorList> Results = new List<VendorList>();
                if (btnShow == "Show All Categories")
                {
                    if (Session["Search"] != null && Session["Search"].ToString() != "")
                    {
                        var param = new DynamicParameters();
                        param.Add("@SearchVendor", Session["Search"].ToString().Trim());
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_filter_vendorcategory", param, commandType: CommandType.StoredProcedure);
                            Results = mpresults.Read<VendorList>().ToList();
                            grdVendorDisplay.CssClass = "grdvendorscroll";
                            grdVendorDisplay.DataSource = Results;
                            grdVendorDisplay.DataBind();
                        }
                    }
                    else
                    {
                        Results = ShowAllCategoriesBind();

                        //bind the items in gridview
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                    }
                }
                else
                {
                    if (Session["Search"] != null && Session["Search"].ToString() != "")
                    {
                        var param = new DynamicParameters();
                        param.Add("@SearchVendor", Session["Search"].ToString().Trim());
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_filter_undefining_vendorcategory", param, commandType: CommandType.StoredProcedure);
                            Results = mpresults.Read<VendorList>().ToList();
                            grdVendorDisplay.CssClass = "grdvendorscroll";
                            grdVendorDisplay.DataSource = Results;
                            grdVendorDisplay.DataBind();
                        }
                    }
                    else
                    {
                        Results = BindDisplayGrid();
                        //bind the items in gridview
                        grdVendorDisplay.DataSource = Results;
                        grdVendorDisplay.DataBind();
                    }
                }
            

        }
    }
}