﻿<%@ Page ValidateRequest="true" EnableEventValidation="true" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateVendorCategory.aspx.cs" Inherits="uusitempricecost.VendorCategory.UpdateVendorCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%-- Declare the Web Service call --%>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/UpdateVendors.asmx" />
        </Services>
    </asp:ScriptManagerProxy>



    <%--Defining the jquery CDN--%>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="Scripts/jquery-3.4.1.js"></script>
    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="Scripts/jquery.loading.block.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/Content/Site1.css" rel="stylesheet" />

    <%-- Alert jquery style start --%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <style type="text/css">
        .jconfirm .jconfirm-box {
            background: white;
            border-radius: 10px;
            position: relative;
            outline: 5px;
        }
    </style>

    <script type="text/javascript">
        //Bootstrap modal popup get and set text box value
        $(document).ready(function () {
            //Bootstrap modal popup get and set text box value
            //Update button(Left side)
            $('.openBtn').on('click', function () {
                $("#overlay").fadeIn(300);
                var dataURL = $(this).attr('data-href');
                $('#myModal').modal({ show: true });
                var VNo = $("#MainContent_txtVendorNumbers").val();
                var VCatNo = $("#MainContent_txtVendorCategory").val();
                var des = $("#MainContent_txt_CategoryDesc").val();

                var VNos = $('#MainContent_txt_VendorNo').val(VNo);
                var VNocats = $('#MainContent_txt_VCategory').val(VCatNo);
                var VDes = $('#MainContent_txt_Desc').val(des);

                $("#modal_body").html(VNos);
                $("#modal_body").html(VNocats);
                $("#modal_body").html(VDes);
                setTimeout(function () {
                    $("#overlay").fadeOut(300);
                }, 500);
            });
            //Update button(Right Side)
            $('.openBtn1').on('click', function () {
                $("#overlay").fadeIn(300);
                var dataURL = $(this).attr('data-href');
                $('#myModal').modal({ show: true });
                var VNo = $("#MainContent_txt_NewVendorNo").val();
                var VCatNo = $("#MainContent_txt_NewVendorCat").val();
                var des = $("#MainContent_txt_newVendorCatDesc").val();

                var VNos = $('#MainContent_txt_VendorNo').val(VNo);
                var VNocats = $('#MainContent_txt_VCategory').val(VCatNo);
                var VDes = $('#MainContent_txt_Desc').val(des);

                $("#modal_body").html(VNos);
                $("#modal_body").html(VNocats);
                $("#modal_body").html(VDes);
                setTimeout(function () {
                    $("#overlay").fadeOut(300);
                }, 500);
            });
        });

        //Modal popup (Update) save the description
        function Save() {
            debugger;
            $("#overlay").fadeIn(300);
            var VNos = $('#MainContent_txt_VendorNo').val();
            var VNocats = $('#MainContent_txt_VCategory').val();
            var VDes = $('#MainContent_txt_Desc').val();

            //ajax call for web service
            $.ajax({
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                //Url is the path of our web method (Page name/function name)
                url: "UpdateVendors.asmx/save_Click",
                //Pass values to parameters. If function have no parameters, then we need to use data: "{ }",
                data: "{'VNos':'" + VNos + "', 'VNocats':'" + VNocats + "', 'VDes':'" + VDes + "'}",
                //called on ajax call success        
                success: function (result) {
                },
                //called on ajax call failure
                error: function (xhr, textStatus, error) {
                    $('#dvMsg').text("Error:" + error);
                }
            });
            $('#myModal').modal('hide');
            if ($('#MainContent_txt_VendorNo').val() == $('#MainContent_txtVendorNumbers').val() && $('#MainContent_txtVendorCategory').val() == $('#MainContent_txt_VCategory').val()) {
                $('#MainContent_txt_CategoryDesc').val(VDes);
            }
            else {
                $('#MainContent_txt_newVendorCatDesc').val(VDes);
            }
            //$('#MainContent_txt_CategoryDesc').val(VDes);
            setTimeout(function () {
                $("#overlay").fadeOut(300);
            }, 5000);



            return false;
        }
        /*JavaScript - Allow only numbers in TextBox
            (Restrict Alphabets and Special Characters).*/
        /*code: 48-57 Numbers*/
        function restrictAlphabets(e) {
            var x = e.which || e.keycode;
            if ((x >= 48 && x <= 57))
                return true;
            else
                return false;
        }

        function fnErrorVendorCategory(VendorNo, VendorCatNo) {
            if (VendorNo != null && VendorNo != undefined && VendorNo != "" && VendorCatNo != null && VendorCatNo != undefined && VendorCatNo != "") {
                $.alert("<b>  Vendor No = " + VendorNo + " and Vendor Category = " + VendorCatNo + " </b> not match the table.Please check and Find it.")
            }
            else {
                $.alert("Must enter the Vendor No,Vendor category and New Vendor No,New Vendor category! ");
            }
        }
        function fnErrorNewVendorCategory(VendorNo, VendorCatNo) {
            if (VendorNo != null && VendorNo != undefined && VendorNo != "" && VendorCatNo != null && VendorCatNo != undefined && VendorCatNo != "") 
                {
                    $.alert("<b> New Vendor No = " + VendorNo + " and New Vendor Category = " + VendorCatNo + " </b> not match the table.Please check and Find it.")
                }
                else {
                    $.alert("Must enter the Vendor No,Vendor category and New Vendor No,New Vendor category! ");
                }
            }
            function fnInValidVendorCategory() {
                $.alert("Must enter the Vendor No,Vendor category and New Vendor No,New Vendor category! ");
            }
            function fnErrorMsg() {
                $.alert("Please select Checkbox then move the item ! ");
            }

            function funNewCategory(event) {
                __doPostBack(event.id);
            }
            function funCategory(event) {
                __doPostBack(event.id);
            }
    </script>

    <div class="update-page-title">
        <div class="container">
            <h3>Update Vendor Categories</h3>
        </div>
    </div>
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <asp:Panel ID="updateCategory" runat="server">
        <div class="container">
            <%--Left side text and update button--%>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-2 visible-lg-block"></div>
                        <div class="col-md-12 col-lg-4">
                            <asp:Label runat="server" ID="lblVendorNo" Text="Vendor Number"></asp:Label>
                            <input runat="server" type="text" name="txtVendorNumber"
                                id="txtVendorNumbers" />
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <asp:Label runat="server" ID="lblVendorCategory" Text="Vendor Category"></asp:Label>
                            <asp:TextBox runat="server" ID="txtVendorCategory"></asp:TextBox>
                        </div>
                        <div class="col-lg-2 visible-lg-block"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-2">
                            <asp:Label runat="server" ID="lbl_Vendor" Text="Vendor"></asp:Label>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <asp:TextBox runat="server" ID="txt_VendorDesc"></asp:TextBox>
                        </div>
                        <div class="col-md-2 visible-lg-block"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-2">
                            <asp:Label runat="server" ID="lbl_CategoryDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <asp:TextBox runat="server" ID="txt_CategoryDesc"></asp:TextBox>
                        </div>
                        <div class="col-md-12 col-lg-2">
                            <button type="button" class="openBtn">Update</button>
                        </div>
                    </div>
                </div>
                <%-- Right Side New Vendors --%>
                <%--New Vendor Desc--%>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-2 visible-lg-block"></div>
                        <div class="col-md-12 col-lg-4">
                            <asp:Label runat="server" ID="lbl_NewVendor" Text="New Vendor Number"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_NewVendorNo"></asp:TextBox>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <asp:Label runat="server" ID="lbl_NewVendorCategory" Text="New Vendor Category"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_NewVendorCat"></asp:TextBox>
                        </div>
                        <div class="col-md-2 visible-lg-block"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-2">
                            <asp:Label runat="server" ID="lbl_NewVendors" Text="Vendor" CssClass="updatelblVen"></asp:Label>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <asp:TextBox runat="server" ID="txt_NewVendor"></asp:TextBox>
                        </div>
                        <div class="col-md-2 visible-lg-block"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-2">
                            <asp:Label runat="server" ID="lbl_newVendorCatDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <asp:TextBox runat="server" ID="txt_newVendorCatDesc"></asp:TextBox>
                        </div>
                        <div class="col-md-12 col-lg-2">
                            <button type="button" class="openBtn1">Update</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </asp:Panel>

    <%--Min and max 100 button--%>
    <panel id="pnlVendorGrid">
        <div class="container">
            <div class="row">
                <div class="col-md-5 text-right">
                    <asp:Button runat="server" ID="btn_min100" Text="-100" OnClick="btn_min100_Click" />
                    <asp:Button runat="server" ID="btn_max100" Text="+100" OnClick="btn_max100_Click" />
                </div>
            </div>
            <%-- Vendor Gridview for left side --%>
            <div class="row">
                <div class="col-md-5">
                    <div style="overflow-y: scroll; max-height: 300px;">
                        <asp:GridView runat="server" ID="grdVendor" AutoGenerateColumns="false" PageSize="10" DataKeyNames="item_no"
                            CssClass="MainContent_grdVendor">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbSelect"
                                            CssClass="gridCB" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
                <%--  Move to New Category--%>
                <div class="col-md-2 updatevendorbtngroup">
                    <div class="movecategory">Move to New Category</div>

                    <asp:ImageButton runat="server" class="arrowbtn" ToolTip=" Move single item" ID="btn_singleLeftArrow" OnClick="btn_singleArrow_Click" ImageUrl="~/Images/arrow_right_icon.svg" ImageAlign="Middle" />
                    <asp:ImageButton runat="server" class="arrowbtn" ToolTip="Move all items" ID="ImageButton2" OnClick="btn_AllRightArrow_Click" ImageUrl="~/Images/double_arrow_right_icon.svg" ImageAlign="Middle" />
                    <asp:ImageButton runat="server" class="arrowbtn" ToolTip=" Move single item" ID="btn_singleArrow" OnClick="btn_singleLeftArrow_Click" ImageUrl="~/Images/arrow_left_icon.svg" ImageAlign="Middle" />
                    <asp:ImageButton runat="server" class="arrowbtn" ToolTip="Move all items" ID="ImageButton1" OnClick="btn_AllLeftArrow_Click" ImageUrl="~/Images/double_arrow_left_icon.svg" ImageAlign="Middle" />

                </div>
                <div class="col-md-5 updatelbl">
                    <div style="overflow-y: scroll; max-height: 300px;">
                        <%-- New Vendor Gridview for Right side --%>
                        <asp:GridView runat="server" ID="grdNewVendor" AutoGenerateColumns="false" PageSize="10" CssClass="MainContent_grdNewVendor">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>

                                            <asp:CheckBox ID="cbCheck" runat="server"  CssClass="gridCB" />
                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </panel>
    </div>
    <%-- Vendors,New Vendors Text box for finding the value --%>
    <asp:Panel runat="server" ID="pnlFind" class="updatecontent">
        <div class="container">
            <div class="updatecontentfootbox">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <div class="col-md-6 col-sm-12">
                            <asp:Label runat="server" ID="lblFndVNo"> Vendor Number</asp:Label>
                            <asp:TextBox runat="server" ID="txt_FndVNo" BackColor="Yellow" Width="110px" Height="40px" onkeypress='return restrictAlphabets(event)'></asp:TextBox>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <asp:Label runat="server" ID="lbl_FndCategory"> Vendor Category</asp:Label>
                            <asp:TextBox runat="server" ID="txt_FndCategory" BackColor="Yellow" Width="110px" Height="40px" onkeypress='return restrictAlphabets(event)' OnTextChanged="txt_FndCategory_TextChanged" AutoPostBack="true" onfocusout="return funCategory(this)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 visible-md-block visible-lg-block"></div>
                    <div class="col-sm-6 col-md-5">
                        <div class="col-md-6 col-sm-12">
                            <asp:Label runat="server" ID="lbl_fndNewVNo"> New Vendor Number</asp:Label>
                            <asp:TextBox runat="server" ID="txt_fndNewVNo" BackColor="Yellow" Width="110px" Height="40px" onkeypress='return restrictAlphabets(event)'></asp:TextBox>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <asp:Label runat="server" ID="lbl_fndNewCategory"> New Vendor Category</asp:Label>
                            <asp:TextBox runat="server" ID="txt_fndNewCategory" BackColor="Yellow" Width="110px" Height="40px" onkeypress='return restrictAlphabets(event)' OnTextChanged="txt_fndNewCategory_TextChanged" onfocusout="return funNewCategory(this)" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <%--Panel for buttons like Find,Reset,Exit  --%>
    <asp:Panel runat="server" ID="pnlButtons" CssClass="pnlbuttons">
        <div class="container">

            <asp:Button runat="server" ID="btn_find" Text="Find It" OnClick="btn_find_Click" />

            <asp:Button runat="server" ID="btn_Reset" Text="Reset" OnClick="btn_Reset_Click" />
            <br />
            <asp:Button runat="server" ID="btn_ViewVendorCat" Text="View Vendor Cat" OnClick="btn_ViewVendorCat_Click" />

            <asp:Button runat="server" ID="btn_Exit" Text="Exit" />
        </div>
    </asp:Panel>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">UPDATE CATEGORY DESCRIPTION</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:Label runat="server" ID="lbl_VNo" Text="Vendor Number"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_VendorNo" BackColor="Yellow" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <asp:Label runat="server" ID="lbl_VCat" Text="Vendor Category"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_VCategory" BackColor="Yellow" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:TextBox runat="server" ID="txt_Desc"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button runat="server" Text="Save" ID="btn_save" OnClientClick="javascript:Save();return false;" class="btn btn-success" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

