﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

namespace uusitempricecost.VendorCategory
{
    public partial class UpdateVendorCategory : System.Web.UI.Page
    {
        #region set,get properties

        public class GridSelect
        {
            public string ItemNote { get; set; }
        }
        public class VendorGrid
        {
            public string item_no { get; set; }
            public string Description { get; set; }
        }
        public class PartNumber
        {
            public string item_no { get; set; }
        }
        public class VendorDetails
        {
            public string VendorNo { get; set; }
            public string VendorCategory { get; set; }
            public string CompanyName { get; set; }
            public string Description { get; set; }
        }
        #endregion

        /// <summary>
        /// Page load - Add global data here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Find text value Initially bind
                txt_FndVNo.Text = Request.QueryString["VendorNo"] != string.Empty ? Request.QueryString["VendorNo"] : txtVendorNumbers.Value; ;
                txt_FndCategory.Text = Request.QueryString["VendorCategory"] != string.Empty ? Request.QueryString["VendorCategory"] : txtVendorCategory.Text;
                txt_fndNewVNo.Text = Request.QueryString["VendorNo"] != string.Empty ? Request.QueryString["VendorNo"] : txt_NewVendorNo.Text;
                txt_fndNewCategory.Text = Request.QueryString["VendorCategory"] != string.Empty ? "99" : txt_NewVendorCat.Text;

                txt_FndVNo.Focus();
                txt_FndVNo.Attributes.Add("onfocusin", " select();");
                BindVendorGrid(sender, e);
                GetTextboxItems(sender, e);
                BindRightVendorGrid(sender, e);
                //Find Vendor category 99  populate the values  null in the right side
            }
            if (IsPostBack)
            {
                string target = Request["__EVENTTARGET"];

                List<bool> IsActive = new List<bool>();
                DataTable dt = new DataTable();
                dt.Columns.Add("item_no");
                dt.Columns.Add("Description");
                foreach (GridViewRow item in grdVendor.Rows)
                {
                    bool chkGrid = (item.Cells[0].FindControl("cbSelect") as CheckBox).Checked;
                    IsActive.Add(chkGrid);
                }
                if (IsActive.Contains(true) == true)
                {
                }
                else
                {
                    if (target == "MainContent_txt_FndCategory")
                    {

                        // Focus out Event via Postback
                        var fndVNo = txt_FndVNo.Text;
                        var fndVCategory = txt_FndCategory.Text;
                        HandleVendorCustomPostbackEvent(fndVNo, fndVCategory);
                    }
                }

                //New Find Vendor check the checkbox
                DataTable dtNew = new DataTable();
                dtNew.Columns.Add("item_no");
                dtNew.Columns.Add("Description");
                foreach (GridViewRow item in grdNewVendor.Rows)
                {
                    bool chkGrid = (item.Cells[0].FindControl("cbCheck") as CheckBox).Checked;
                    IsActive.Add(chkGrid);
                }
                if (IsActive.Contains(true) == true)
                {
                }
                else
                {
                    if (target == "MainContent_txt_fndNewCategory")
                    {
                        // Focus out Event via Postback
                        var fndNewVNo = txt_fndNewVNo.Text;
                        var fndNewVCategory = txt_fndNewCategory.Text;
                        HandleCustomPostbackEvent(fndNewVNo, fndNewVCategory);
                    }
                }
            }
        }

        #region Postback handling
        public void HandleVendorCustomPostbackEvent(string VendorNo, string VendorCatNo)
        {
            List<VendorDetails> vendorDetails = new List<VendorDetails>();
            List<VendorDetails> vendorDetail = new List<VendorDetails>();
            List<VendorGrid> Results = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();

            var param = new DynamicParameters();
            param.Add("@VendorNo", VendorNo);
            param.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_find_vendor", param, commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorGrid>().ToList();
                if (Results.Count > 0)
                {
                    //Left Panel
                    if (VendorNo != string.Empty && VendorCatNo != string.Empty)
                    {
                        using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresult = dbs.QueryMultiple("sp_get_update_vendorgrid", param, commandType: CommandType.StoredProcedure);
                            Results = mpresult.Read<VendorGrid>().ToList();
                            grdVendor.DataSource = Results;
                            grdVendor.DataBind();

                            //Bind Text box Value
                            var mpresult1 = db.QueryMultiple("sp_getupdateitems", param, commandType: CommandType.StoredProcedure);
                            vendorDetails = mpresult1.Read<VendorDetails>().ToList();

                            if (vendorDetails.Count > 0)
                            {
                                //Vendor item set in text boxes
                                txtVendorNumbers.Value = vendorDetails.FirstOrDefault().VendorNo != null ? vendorDetails.FirstOrDefault().VendorNo : string.Empty;
                                txtVendorCategory.Text = vendorDetails.FirstOrDefault().VendorCategory != null ? vendorDetails.FirstOrDefault().VendorCategory : string.Empty;
                                txt_VendorDesc.Text = vendorDetails.FirstOrDefault().CompanyName != null ? vendorDetails.FirstOrDefault().CompanyName : string.Empty;
                                txt_CategoryDesc.Text = vendorDetails.FirstOrDefault().Description != null ? vendorDetails.FirstOrDefault().Description : string.Empty;
                            }
                        }
                    }
                }
                else
                {
                    string jsFunc = "fnErrorVendorCategory(" + VendorNo + "," + VendorCatNo + ")";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                    return;
                }
            }
            if (VendorCatNo == "99")
            {
                txt_NewVendorNo.Text = string.Empty;
                txt_NewVendorCat.Text = string.Empty;
                txt_NewVendor.Text = string.Empty;
                txt_newVendorCatDesc.Text = string.Empty;

                //Find text control set empty
                txt_fndNewVNo.Text = string.Empty;
                txt_fndNewCategory.Text = string.Empty;

                //Grid set null
                grdNewVendor.DataSource = null;
                grdNewVendor.DataBind();
            }
            else
            {
                using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                {
                    string newVendor_No = VendorNo;
                    string newVendor_Category = "99";
                    var param1 = new DynamicParameters();
                    param1.Add("@VendorNo", newVendor_No);
                    param1.Add("@VendorCategoryNo", newVendor_Category);

                    var param2 = new DynamicParameters();
                    param2.Add("@newVendorNo ", newVendor_No);
                    param2.Add("@newVendorCatNo", newVendor_Category);

                    var mpresult = dbs.QueryMultiple("sp_find_vendor", param1, commandType: CommandType.StoredProcedure);
                    Results = mpresult.Read<VendorGrid>().ToList();
                    if (Results.Count > 0)
                    {
                        if (newVendor_No != string.Empty && newVendor_Category != string.Empty)
                        {
                            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                            {
                                var mpresult1 = db.QueryMultiple("sp_get_rightvendorgrid", param1, commandType: CommandType.StoredProcedure);
                                Result = mpresult1.Read<VendorGrid>().ToList();
                                grdNewVendor.DataSource = Result;
                                grdNewVendor.DataBind();

                                var mpResult = db.QueryMultiple("sp_getRightupdateitems", param2, commandType: CommandType.StoredProcedure);
                                vendorDetail = mpResult.Read<VendorDetails>().ToList();

                                if (vendorDetail.Count > 0)
                                {
                                    // New Vendor item set in text boxes
                                    txt_NewVendorNo.Text = vendorDetail.FirstOrDefault().VendorNo != null ? vendorDetail.FirstOrDefault().VendorNo : string.Empty;
                                    txt_NewVendorCat.Text = vendorDetail.FirstOrDefault().VendorCategory != null ? vendorDetail.FirstOrDefault().VendorCategory : string.Empty;
                                    txt_NewVendor.Text = vendorDetail.FirstOrDefault().CompanyName != null ? vendorDetail.FirstOrDefault().CompanyName : string.Empty;
                                    txt_newVendorCatDesc.Text = vendorDetail.FirstOrDefault().Description != null ? vendorDetail.FirstOrDefault().Description : string.Empty;
                                    txt_fndNewVNo.Text = newVendor_No;
                                    txt_fndNewCategory.Text = newVendor_Category;
                                }
                            }
                        }
                    }
                    else
                    {
                        string jsFunc = "fnErrorNewVendorCategory(" + newVendor_No + "," + newVendor_Category + ")";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                        return;
                    }
                }
            }
        }
        public void HandleCustomPostbackEvent(string fndNewVNo, string fndNewVCategory)
        {
            List<VendorDetails> vendorDetail = new List<VendorDetails>();
            List<VendorGrid> Results = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();

            string newVendor_No = fndNewVNo;
            string newVendor_Categoty = fndNewVCategory;

            using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var param1 = new DynamicParameters();
                param1.Add("@VendorNo", newVendor_No);
                param1.Add("@VendorCategoryNo", newVendor_Categoty);

                var param2 = new DynamicParameters();
                param2.Add("@newVendorNo ", newVendor_No);
                param2.Add("@newVendorCatNo", newVendor_Categoty);

                var mpresult = dbs.QueryMultiple("sp_find_vendor", param1, commandType: CommandType.StoredProcedure);
                Results = mpresult.Read<VendorGrid>().ToList();
                if (Results.Count > 0)
                {
                    if (newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
                    {
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresult1 = db.QueryMultiple("sp_get_rightvendorgrid", param1, commandType: CommandType.StoredProcedure);
                            Result = mpresult1.Read<VendorGrid>().ToList();
                            grdNewVendor.DataSource = Result;
                            grdNewVendor.DataBind();

                            var mpResult = db.QueryMultiple("sp_getRightupdateitems", param2, commandType: CommandType.StoredProcedure);
                            vendorDetail = mpResult.Read<VendorDetails>().ToList();

                            if (vendorDetail.Count > 0)
                            {
                                // New Vendor item set in text boxes
                                txt_NewVendorNo.Text = vendorDetail.FirstOrDefault().VendorNo != null ? vendorDetail.FirstOrDefault().VendorNo : string.Empty;
                                txt_NewVendorCat.Text = vendorDetail.FirstOrDefault().VendorCategory != null ? vendorDetail.FirstOrDefault().VendorCategory : string.Empty;
                                txt_NewVendor.Text = vendorDetail.FirstOrDefault().CompanyName != null ? vendorDetail.FirstOrDefault().CompanyName : string.Empty;
                                txt_newVendorCatDesc.Text = vendorDetail.FirstOrDefault().Description != null ? vendorDetail.FirstOrDefault().Description : string.Empty;
                            }
                        }
                    }
                }
                else
                {
                    string jsFunc = "fnErrorNewVendorCategory(" + newVendor_No + "," + newVendor_Categoty + ")";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                    return;
                }
                btn_find.Focus();
            }
        }
        #endregion

        #region Left side grid
        public List<VendorGrid> BindVendorGrid(object sender, EventArgs e)
        {
            List<VendorGrid> Results = new List<VendorGrid>();

            string VendorNo = Request.QueryString["VendorNo"];
            string VendorCatNo = Request.QueryString["VendorCategory"];

            var param = new DynamicParameters();
            param.Add("@VendorNo", VendorNo);
            param.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param, commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = Results;
                grdVendor.DataBind();

            }
            return Results;
        }
        #endregion

        #region Right side grid
        public List<VendorGrid> BindRightVendorGrid(object sender, EventArgs e)
        {
            List<VendorGrid> Results = new List<VendorGrid>();

            string VendorNo = Request.QueryString["VendorNo"];
            string VendorCatNo = "99";
            string CatNo = Request.QueryString["VendorCategory"];
            if (CatNo == "99")
            {
                txt_NewVendorNo.Text = string.Empty;
                txt_NewVendorCat.Text = string.Empty;
                txt_NewVendor.Text = string.Empty;
                txt_newVendorCatDesc.Text = string.Empty;

                //Find text control set empty
                txt_fndNewVNo.Text = string.Empty;
                txt_fndNewCategory.Text = string.Empty;

                //Grid set null
                grdNewVendor.DataSource = null;
                grdNewVendor.DataBind();
            }
            else
            {
                var param = new DynamicParameters();
                param.Add("@VendorNo", VendorNo);
                param.Add("@VendorCategoryNo", VendorCatNo);
                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                {
                    var mpresults = db.QueryMultiple("sp_get_rightvendorgrid", param, commandType: CommandType.StoredProcedure);
                    Results = mpresults.Read<VendorGrid>().ToList();
                    grdNewVendor.DataSource = Results;
                    grdNewVendor.DataBind();
                }
            }

            return Results;
        }
        #endregion

        #region Get and set Right and Left side values
        public List<VendorDetails> GetTextboxItems(object sender, EventArgs e)
        {
            //left and right text values get from sp
            List<VendorDetails> Results = new List<VendorDetails>();
            List<VendorDetails> Result = new List<VendorDetails>();

            //Get Vendor and Category No in textbox or querystring
            string VendorNo = Request.QueryString["VendorNo"] != string.Empty ? Request.QueryString["VendorNo"] : txtVendorNumbers.Value;
            string VendorCatNo = Request.QueryString["VendorCategory"] != string.Empty ? Request.QueryString["VendorCategory"] : txtVendorCategory.Text;

            //Get New Vendor and New Category No in textbox or querystring
            string newVendorNo = Request.QueryString["VendorNo"] != string.Empty ? Request.QueryString["VendorNo"] : txt_NewVendorNo.Text;
            string newVendorCatNo = Request.QueryString["VendorCategory"] != string.Empty ? "99" : txt_NewVendorCat.Text;

            //Set Param for Vendor and Category
            var param = new DynamicParameters();
            param.Add("@VendorNo", VendorNo);
            param.Add("@VendorCategoryNo", VendorCatNo);

            //Set  Param for New Vendor and New Category
            var param1 = new DynamicParameters();
            param1.Add("@newVendorNo", newVendorNo);
            param1.Add("@newVendorCatNo", newVendorCatNo);

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_getupdateitems", param, commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorDetails>().ToList();
            }

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {

                var mpresults = db.QueryMultiple("sp_getRightupdateitems", param1, commandType: CommandType.StoredProcedure);
                Result = mpresults.Read<VendorDetails>().ToList();
            }
            if (Results.Count > 0)
            {
                //Vendor item set in text boxes
                txtVendorNumbers.Value = Results.FirstOrDefault().VendorNo != null ? Results.FirstOrDefault().VendorNo : string.Empty;
                txtVendorCategory.Text = Results.FirstOrDefault().VendorCategory != null ? Results.FirstOrDefault().VendorCategory : string.Empty;
                txt_VendorDesc.Text = Results.FirstOrDefault().CompanyName != null ? Results.FirstOrDefault().CompanyName : string.Empty;
                txt_CategoryDesc.Text = Results.FirstOrDefault().Description != null ? Results.FirstOrDefault().Description : string.Empty;
            }

            if (Result.Count > 0)
            {
                // New Vendor item set in text boxes
                txt_NewVendorNo.Text = Result.FirstOrDefault().VendorNo != null ? Result.FirstOrDefault().VendorNo : string.Empty;
                txt_NewVendorCat.Text = Result.FirstOrDefault().VendorCategory != null ? Result.FirstOrDefault().VendorCategory : string.Empty;
                txt_NewVendor.Text = Result.FirstOrDefault().CompanyName != null ? Result.FirstOrDefault().CompanyName : string.Empty;
                txt_newVendorCatDesc.Text = Result.FirstOrDefault().Description != null ? Result.FirstOrDefault().Description : string.Empty;
            }
            return Results;
        }
        #endregion

        #region Minimum 100 rows display
        protected void btn_min100_Click(object sender, EventArgs e)
        {
            List<VendorGrid> Results = new List<VendorGrid>();

            string VendorNo = txtVendorNumbers.Value;
            string VendorCatNo = txtVendorCategory.Text;

            var param = new DynamicParameters();
            param.Add("@VendorNo", VendorNo);
            param.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_min_gridvalues", param, commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = Results;
                grdVendor.DataBind();
            }
        }
        #endregion

        #region Maximum 100 rows

        protected void btn_max100_Click(object sender, EventArgs e)
        {
            List<VendorGrid> Results = new List<VendorGrid>();

            string VendorNo = txtVendorNumbers.Value;
            string VendorCatNo = txtVendorCategory.Text;

            var param = new DynamicParameters();
            param.Add("@VendorNo", VendorNo);
            param.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_max_gridvalues", param, commandType: CommandType.StoredProcedure);
                Results = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = Results;
                grdVendor.DataBind();

            }
        }
        #endregion

        #region buttons
        protected void btn_ViewVendorCat_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            txt_FndVNo.Text = string.Empty;
            txt_FndCategory.Text = string.Empty;
            txt_fndNewVNo.Text = string.Empty;
            txt_fndNewCategory.Text = string.Empty;

            txtVendorNumbers.Value = string.Empty;
            txtVendorCategory.Text = string.Empty;
            txt_VendorDesc.Text = string.Empty;
            txt_CategoryDesc.Text = string.Empty;

            txt_NewVendorNo.Text = string.Empty;
            txt_NewVendorCat.Text = string.Empty;
            txt_NewVendor.Text = string.Empty;
            txt_newVendorCatDesc.Text = string.Empty;

            grdVendor.DataSource = null;
            grdVendor.DataBind();
            grdNewVendor.DataSource = null;
            grdNewVendor.DataBind();
            txt_FndVNo.Focus();
        }
        protected void btn_find_Click(object sender, EventArgs e)
        {
            List<VendorDetails> vendorDetails = new List<VendorDetails>();
            List<VendorDetails> vendorDetail = new List<VendorDetails>();
            List<VendorGrid> Results = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();
            string VendorNo = txt_FndVNo.Text;
            string VendorCatNo = txt_FndCategory.Text;

            string newVendor_No = txt_fndNewVNo.Text;
            string newVendor_Categoty = txt_fndNewCategory.Text;
            if (VendorNo != string.Empty && VendorCatNo != string.Empty && newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
            {
                var param = new DynamicParameters();
                param.Add("@VendorNo", VendorNo);
                param.Add("@VendorCategoryNo", VendorCatNo);
                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                {
                    var mpresults = db.QueryMultiple("sp_find_vendor", param, commandType: CommandType.StoredProcedure);
                    Results = mpresults.Read<VendorGrid>().ToList();
                    if (Results.Count > 0)
                    {
                        //Left Panel
                        if (VendorNo != string.Empty && VendorCatNo != string.Empty)
                        {
                            using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                            {
                                var mpresult = dbs.QueryMultiple("sp_get_update_vendorgrid", param, commandType: CommandType.StoredProcedure);
                                Results = mpresult.Read<VendorGrid>().ToList();
                                grdVendor.DataSource = Results;
                                grdVendor.DataBind();

                                //Bind Text box Value
                                var mpresult1 = db.QueryMultiple("sp_getupdateitems", param, commandType: CommandType.StoredProcedure);
                                vendorDetails = mpresult1.Read<VendorDetails>().ToList();

                                if (vendorDetails.Count > 0)
                                {
                                    //Vendor item set in text boxes
                                    txtVendorNumbers.Value = vendorDetails.FirstOrDefault().VendorNo != null ? vendorDetails.FirstOrDefault().VendorNo : string.Empty;
                                    txtVendorCategory.Text = vendorDetails.FirstOrDefault().VendorCategory != null ? vendorDetails.FirstOrDefault().VendorCategory : string.Empty;
                                    txt_VendorDesc.Text = vendorDetails.FirstOrDefault().CompanyName != null ? vendorDetails.FirstOrDefault().CompanyName : string.Empty;
                                    txt_CategoryDesc.Text = vendorDetails.FirstOrDefault().Description != null ? vendorDetails.FirstOrDefault().Description : string.Empty;
                                }
                            }
                        }
                    }
                    else
                    {
                        string jsFunc = "fnErrorVendorCategory(" + VendorNo + "," + VendorCatNo + ")";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                        return;
                    }
                }
            }


            //Right Panel
            using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var param1 = new DynamicParameters();
                param1.Add("@VendorNo", newVendor_No);
                param1.Add("@VendorCategoryNo", newVendor_Categoty);

                var param2 = new DynamicParameters();
                param2.Add("@newVendorNo ", newVendor_No);
                param2.Add("@newVendorCatNo", newVendor_Categoty);

                var mpresult = dbs.QueryMultiple("sp_find_vendor", param1, commandType: CommandType.StoredProcedure);
                Results = mpresult.Read<VendorGrid>().ToList();
                if (Results.Count > 0)
                {
                    if (newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
                    {
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresults = db.QueryMultiple("sp_get_rightvendorgrid", param1, commandType: CommandType.StoredProcedure);
                            Result = mpresults.Read<VendorGrid>().ToList();
                            grdNewVendor.DataSource = Result;
                            grdNewVendor.DataBind();

                            var mpResult = db.QueryMultiple("sp_getRightupdateitems", param2, commandType: CommandType.StoredProcedure);
                            vendorDetail = mpResult.Read<VendorDetails>().ToList();

                            if (vendorDetail.Count > 0)
                            {
                                // New Vendor item set in text boxes
                                txt_NewVendorNo.Text = vendorDetail.FirstOrDefault().VendorNo != null ? vendorDetail.FirstOrDefault().VendorNo : string.Empty;
                                txt_NewVendorCat.Text = vendorDetail.FirstOrDefault().VendorCategory != null ? vendorDetail.FirstOrDefault().VendorCategory : string.Empty;
                                txt_NewVendor.Text = vendorDetail.FirstOrDefault().CompanyName != null ? vendorDetail.FirstOrDefault().CompanyName : string.Empty;
                                txt_newVendorCatDesc.Text = vendorDetail.FirstOrDefault().Description != null ? vendorDetail.FirstOrDefault().Description : string.Empty;
                            }
                        }
                    }
                }

            }
            txt_fndNewCategory.Focus();
        }
        #endregion

        #region button for Move Category
        // Left to Right Arrow click event
        protected void btn_singleArrow_Click(object sender, EventArgs e)
        {
            List<VendorGrid> VendorGrid = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();
            List<GridSelect> Results = new List<GridSelect>();
            string value = string.Empty;
            string VendorNo = string.Empty;
            string VendorCatNo = string.Empty;
            string newVendorNo = string.Empty;
            string newVendorCatNo = string.Empty;
            string partNumber = String.Empty;
            string description = String.Empty;
            string newValue = string.Empty;
            string PartNumber = string.Empty;
            List<string> parts = new List<string>();

            DataTable dt = new DataTable();
            dt.Columns.Add("item_no");
            dt.Columns.Add("Description");

            foreach (GridViewRow item in grdVendor.Rows)
            {
                if ((item.Cells[0].FindControl("cbSelect") as CheckBox).Checked)
                {
                    DataRow dr = dt.NewRow();
                    PartNumber = item.Cells[1].Text;
                    parts.Add(PartNumber);
                }
            }
          
            //Right side value
            newVendorNo = txt_NewVendorNo.Text;
            newVendorCatNo = txt_NewVendorCat.Text;
            newValue = newVendorNo + "." + newVendorCatNo;

            //Left side value
            VendorCatNo = txtVendorCategory.Text;
            VendorNo = txtVendorNumbers.Value;
            value = VendorNo + "." + VendorCatNo;

            string joinParts = string.Join(",", parts);
            //Update the single arrow
            var param1 = new DynamicParameters();
            param1.Add("@Item_Note1", value);
            param1.Add("@Item_No", joinParts.Trim());
            param1.Add("@VendorNo", newVendorNo);
            param1.Add("@Item_Note", newValue);

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.Execute("sp_update_rightside_arrow", param1, commandType: CommandType.StoredProcedure);
            }

            //Get the Left and right grid values based the new values
            var param = new DynamicParameters();
            param.Add("@VendorNo", newVendorNo);
            param.Add("@VendorCategoryNo", newVendorCatNo);

            var param2 = new DynamicParameters();
            param2.Add("@VendorNo", VendorNo);
            param2.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param2, commandType: CommandType.StoredProcedure);
                VendorGrid = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = VendorGrid;
                grdVendor.DataBind();
            }
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresult = db.QueryMultiple("sp_get_rightvendorgrid", param, commandType: CommandType.StoredProcedure);
                Result = mpresult.Read<VendorGrid>().ToList();
                grdNewVendor.DataSource = Result;
                grdNewVendor.DataBind();

            }
        }
        //Right to Left arrow click event
        protected void btn_singleLeftArrow_Click(object sender, ImageClickEventArgs e)
        {
            List<VendorGrid> VendorGrid = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();
            List<GridSelect> Results = new List<GridSelect>();
            string value = string.Empty;
            string VendorNo = string.Empty;
            string VendorCatNo = string.Empty;
            string newVendorNo = string.Empty;
            string newVendorCatNo = string.Empty;
            string partNumber = String.Empty;
            string description = String.Empty;
            string newValue = string.Empty;
            string PartNumber = string.Empty;

            List<string> parts = new List<string>();

            DataTable dt = new DataTable();
            dt.Columns.Add("item_no");
            dt.Columns.Add("Description");

            foreach (GridViewRow item in grdNewVendor.Rows)
            {
                if ((item.Cells[0].FindControl("cbCheck") as CheckBox).Checked)
                {
                    DataRow dr = dt.NewRow();
                    PartNumber = item.Cells[1].Text;
                    parts.Add(PartNumber);
                }
            }
            //Left side value
            newVendorNo = txt_NewVendorNo.Text;
            newVendorCatNo = txt_NewVendorCat.Text;
            newValue = newVendorNo + "." + newVendorCatNo;

            //Right side value
            VendorCatNo = txtVendorCategory.Text;
            VendorNo = txtVendorNumbers.Value;
            value = VendorNo + "." + VendorCatNo;

            string joinParts = string.Join(",", parts);
            //Update the single arrow
            var param1 = new DynamicParameters();
            param1.Add("@Item_Note1", newValue);
            param1.Add("@Item_No", joinParts.Trim());
            param1.Add("@VendorNo", VendorNo);
            param1.Add("@Item_Note", value);


            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.Execute("sp_update_rightside_arrow", param1, commandType: CommandType.StoredProcedure);
            }

            //Get the Left and right grid values based the new values
            var param = new DynamicParameters();
            param.Add("@VendorNo", newVendorNo);
            param.Add("@VendorCategoryNo", newVendorCatNo);

            var param2 = new DynamicParameters();
            param2.Add("@VendorNo", VendorNo);
            param2.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param2, commandType: CommandType.StoredProcedure);
                VendorGrid = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = VendorGrid;
                grdVendor.DataBind();
            }
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresult = db.QueryMultiple("sp_get_rightvendorgrid", param, commandType: CommandType.StoredProcedure);
                Result = mpresult.Read<VendorGrid>().ToList();
                grdNewVendor.DataSource = Result;
                grdNewVendor.DataBind();
            }


        }

        //All item moved to Left to Right
        protected void btn_AllRightArrow_Click(object sender, ImageClickEventArgs e)
        {
            List<VendorGrid> VendorGrid = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();
            string value = string.Empty;
            string VendorNo = string.Empty;
            string VendorCatNo = string.Empty;
            string newVendorNo = string.Empty;
            string newVendorCatNo = string.Empty;
            string partNumber = String.Empty;
            string description = String.Empty;
            string newValue = string.Empty;
            List<PartNumber> Results = new List<PartNumber>();
            List<string> parts = new List<string>();

            newVendorNo = txt_NewVendorNo.Text;
            newVendorCatNo = txt_NewVendorCat.Text;
            newValue = newVendorNo + "." + newVendorCatNo;

            VendorNo = txtVendorNumbers.Value;
            VendorCatNo = txtVendorCategory.Text;
            value = VendorNo + "." + VendorCatNo;

            DataTable dt = new DataTable();
            dt.Columns.Add("item_no");
            dt.Columns.Add("Description");

            var param3 = new DynamicParameters();
            param3.Add("@VendorNo", VendorNo);
            param3.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param3, commandType: CommandType.StoredProcedure);
                parts = mpresults.Read<string>().ToList();
                grdVendor.DataSource = parts;
            }
            //Add the Comma separate
            string joinParts = string.Join(",", parts);

            var param1 = new DynamicParameters();
            param1.Add("@Item_Note1", newValue);
            param1.Add("@Item_Note", value);
            param1.Add("@VendorNo", newVendorNo);
            param1.Add("@Item_No", joinParts.Trim());
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.Execute("sp_update_Allrightside_arrow", param1, commandType: CommandType.StoredProcedure);
            }

            //Get the Left and right grid values based the new values
            var param = new DynamicParameters();
            param.Add("@VendorNo", newVendorNo);
            param.Add("@VendorCategoryNo", newVendorCatNo);

            var param2 = new DynamicParameters();
            param2.Add("@VendorNo", VendorNo);
            param2.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param2, commandType: CommandType.StoredProcedure);
                VendorGrid = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = VendorGrid;
                grdVendor.DataBind();
            }
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresult = db.QueryMultiple("sp_get_rightvendorgrid", param, commandType: CommandType.StoredProcedure);
                Result = mpresult.Read<VendorGrid>().ToList();
                grdNewVendor.DataSource = Result;
                grdNewVendor.DataBind();
            }
        }
        protected void btn_AllLeftArrow_Click(object sender, ImageClickEventArgs e)
        {
            List<VendorGrid> VendorGrid = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();
            string value = string.Empty;
            string VendorNo = string.Empty;
            string VendorCatNo = string.Empty;
            string newVendorNo = string.Empty;
            string newVendorCatNo = string.Empty;
            string partNumber = String.Empty;
            string description = String.Empty;
            string newValue = string.Empty;

            List<PartNumber> Results = new List<PartNumber>();
            List<string> parts = new List<string>();

            newVendorNo = txt_NewVendorNo.Text;
            newVendorCatNo = txt_NewVendorCat.Text;
            newValue = newVendorNo + "." + newVendorCatNo;

            VendorNo = txtVendorNumbers.Value;
            VendorCatNo = txtVendorCategory.Text;
            value = VendorNo + "." + VendorCatNo;

            DataTable dt = new DataTable();
            dt.Columns.Add("item_no");
            dt.Columns.Add("Description");

            var param3 = new DynamicParameters();
            param3.Add("@VendorNo", newVendorNo);
            param3.Add("@VendorCategoryNo", newVendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_rightvendorgrid", param3, commandType: CommandType.StoredProcedure);
                parts = mpresults.Read<string>().ToList();
                grdNewVendor.DataSource = parts;
            }
            //Add the Comma separate
            string joinParts = string.Join(",", parts);

            var param1 = new DynamicParameters();
            param1.Add("@Item_Note1", value);
            param1.Add("@Item_Note", newValue);
            param1.Add("@VendorNo", VendorNo);
            param1.Add("@Item_No", joinParts.Trim());
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.Execute("sp_update_Allleftside_arrow", param1, commandType: CommandType.StoredProcedure);
            }
            //Get the Left and right grid values based the new values
            var param = new DynamicParameters();
            param.Add("@VendorNo", newVendorNo);
            param.Add("@VendorCategoryNo", newVendorCatNo);

            var param2 = new DynamicParameters();
            param2.Add("@VendorNo", VendorNo);
            param2.Add("@VendorCategoryNo", VendorCatNo);
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.QueryMultiple("sp_get_update_vendorgrid", param2, commandType: CommandType.StoredProcedure);
                VendorGrid = mpresults.Read<VendorGrid>().ToList();
                grdVendor.DataSource = VendorGrid;
                grdVendor.DataBind();
            }
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresult = db.QueryMultiple("sp_get_rightvendorgrid", param, commandType: CommandType.StoredProcedure);
                Result = mpresult.Read<VendorGrid>().ToList();
                grdNewVendor.DataSource = Result;
                grdNewVendor.DataBind();
            }
        }
        #endregion

        #region the New Vendor Number = Vendor Number  (10)   and New Vendor Category = 99 and populates the upper sections
        protected void txt_FndCategory_TextChanged(object sender, EventArgs e)
        {
            if (txt_FndVNo.Text != string.Empty && txt_FndCategory.Text != string.Empty)
            {
                List<VendorDetails> vendorDetails = new List<VendorDetails>();
                List<VendorDetails> vendorDetail = new List<VendorDetails>();
                List<VendorGrid> Results = new List<VendorGrid>();
                List<VendorGrid> Result = new List<VendorGrid>();
                string VendorNo = txt_FndVNo.Text;
                string VendorCatNo = txt_FndCategory.Text;

                string newVendor_No = txt_FndVNo.Text;
                string newVendor_Categoty = "99";

                txt_fndNewVNo.Text = newVendor_No;
                txt_fndNewCategory.Text = "99";

                if (VendorNo != string.Empty && VendorCatNo != string.Empty && newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
                {
                    var param = new DynamicParameters();
                    param.Add("@VendorNo", VendorNo);
                    param.Add("@VendorCategoryNo", VendorCatNo);
                    using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                    {
                        var mpresults = db.QueryMultiple("sp_find_vendor", param, commandType: CommandType.StoredProcedure);
                        Results = mpresults.Read<VendorGrid>().ToList();
                        if (Results.Count > 0)
                        {
                            //Left Panel
                            if (VendorNo != "" && VendorCatNo != "")
                            {
                                using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                                {
                                    var mpresult = dbs.QueryMultiple("sp_get_update_vendorgrid", param, commandType: CommandType.StoredProcedure);
                                    Results = mpresult.Read<VendorGrid>().ToList();
                                    grdVendor.DataSource = Results;
                                    grdVendor.DataBind();

                                    //Bind Text box Value
                                    var mpresult1 = db.QueryMultiple("sp_getupdateitems", param, commandType: CommandType.StoredProcedure);
                                    vendorDetails = mpresult1.Read<VendorDetails>().ToList();

                                    if (vendorDetails.Count > 0)
                                    {
                                        //Vendor item set in text boxes
                                        txtVendorNumbers.Value = vendorDetails.FirstOrDefault().VendorNo != null ? vendorDetails.FirstOrDefault().VendorNo : "";
                                        txtVendorCategory.Text = vendorDetails.FirstOrDefault().VendorCategory != null ? vendorDetails.FirstOrDefault().VendorCategory : "";
                                        txt_VendorDesc.Text = vendorDetails.FirstOrDefault().CompanyName != null ? vendorDetails.FirstOrDefault().CompanyName : "";
                                        txt_CategoryDesc.Text = vendorDetails.FirstOrDefault().Description != null ? vendorDetails.FirstOrDefault().Description : "";
                                    }
                                }
                            }
                        }
                        else
                        {
                            string jsFunc = "fnErrorVendorCategory(" + VendorNo + "," + VendorCatNo + ")";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                            return;
                        }
                    }
                }
                else
                {
                    string jsFunc = "fnInValidVendorCategory()";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                    return;
                }
                using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                {
                    var param1 = new DynamicParameters();
                    param1.Add("@VendorNo", newVendor_No);
                    param1.Add("@VendorCategoryNo", newVendor_Categoty);

                    var param2 = new DynamicParameters();
                    param2.Add("@newVendorNo ", newVendor_No);
                    param2.Add("@newVendorCatNo", newVendor_Categoty);

                    var mpresult = dbs.QueryMultiple("sp_find_vendor", param1, commandType: CommandType.StoredProcedure);
                    Results = mpresult.Read<VendorGrid>().ToList();
                    if (Results.Count > 0)
                    {
                        if (newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
                        {
                            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                            {
                                var mpresult1 = db.QueryMultiple("sp_get_rightvendorgrid", param1, commandType: CommandType.StoredProcedure);
                                Result = mpresult1.Read<VendorGrid>().ToList();
                                grdNewVendor.DataSource = Result;
                                grdNewVendor.DataBind();

                                var mpResult = db.QueryMultiple("sp_getRightupdateitems", param2, commandType: CommandType.StoredProcedure);
                                vendorDetail = mpResult.Read<VendorDetails>().ToList();

                                if (vendorDetail.Count > 0)
                                {
                                    // New Vendor item set in text boxes
                                    txt_NewVendorNo.Text = vendorDetail.FirstOrDefault().VendorNo != null ? vendorDetail.FirstOrDefault().VendorNo : string.Empty;
                                    txt_NewVendorCat.Text = vendorDetail.FirstOrDefault().VendorCategory != null ? vendorDetail.FirstOrDefault().VendorCategory : string.Empty;
                                    txt_NewVendor.Text = vendorDetail.FirstOrDefault().CompanyName != null ? vendorDetail.FirstOrDefault().CompanyName : string.Empty;
                                    txt_newVendorCatDesc.Text = vendorDetail.FirstOrDefault().Description != null ? vendorDetail.FirstOrDefault().Description : string.Empty;
                                }
                            }
                        }
                    }
                    else
                    {
                        string jsFunc = "fnErrorNewVendorCategory(" + newVendor_No + "," + newVendor_Categoty + ")";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                        return;
                    }
                }
                txt_FndCategory.Focus();
            }

            //Left side Find Vendor Category is 99 populate the right side as empty or null
            if (txt_FndCategory.Text == "99")
            {
                txt_NewVendorNo.Text = string.Empty;
                txt_NewVendorCat.Text = string.Empty;
                txt_NewVendor.Text = string.Empty;
                txt_newVendorCatDesc.Text = string.Empty;

                //Find text control set empty
                txt_fndNewVNo.Text = string.Empty;
                txt_fndNewCategory.Text = string.Empty;

                //Grid set null
                grdNewVendor.DataSource = null;
                grdNewVendor.DataBind();
            }
        }
        protected void txt_fndNewCategory_TextChanged(object sender, EventArgs e)
        {
            List<VendorDetails> vendorDetail = new List<VendorDetails>();
            List<VendorGrid> Results = new List<VendorGrid>();
            List<VendorGrid> Result = new List<VendorGrid>();

            string newVendor_No = txt_fndNewVNo.Text;
            string newVendor_Categoty = txt_fndNewCategory.Text;

            using (var dbs = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var param1 = new DynamicParameters();
                param1.Add("@VendorNo", newVendor_No);
                param1.Add("@VendorCategoryNo", newVendor_Categoty);

                var param2 = new DynamicParameters();
                param2.Add("@newVendorNo ", newVendor_No);
                param2.Add("@newVendorCatNo", newVendor_Categoty);

                var mpresult = dbs.QueryMultiple("sp_find_vendor", param1, commandType: CommandType.StoredProcedure);
                Results = mpresult.Read<VendorGrid>().ToList();
                if (Results.Count > 0)
                {
                    if (newVendor_No != string.Empty && newVendor_Categoty != string.Empty)
                    {
                        using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
                        {
                            var mpresult1 = db.QueryMultiple("sp_get_rightvendorgrid", param1, commandType: CommandType.StoredProcedure);
                            Result = mpresult1.Read<VendorGrid>().ToList();
                            grdNewVendor.DataSource = Result;
                            grdNewVendor.DataBind();

                            var mpResult = db.QueryMultiple("sp_getRightupdateitems", param2, commandType: CommandType.StoredProcedure);
                            vendorDetail = mpResult.Read<VendorDetails>().ToList();

                            if (vendorDetail.Count > 0)
                            {
                                // New Vendor item set in text boxes
                                txt_NewVendorNo.Text = vendorDetail.FirstOrDefault().VendorNo != null ? vendorDetail.FirstOrDefault().VendorNo : string.Empty;
                                txt_NewVendorCat.Text = vendorDetail.FirstOrDefault().VendorCategory != null ? vendorDetail.FirstOrDefault().VendorCategory : string.Empty;
                                txt_NewVendor.Text = vendorDetail.FirstOrDefault().CompanyName != null ? vendorDetail.FirstOrDefault().CompanyName : string.Empty;
                                txt_newVendorCatDesc.Text = vendorDetail.FirstOrDefault().Description != null ? vendorDetail.FirstOrDefault().Description : string.Empty;
                            }
                        }
                    }
                }
                else
                {
                    string jsFunc = "fnErrorNewVendorCategory(" + newVendor_No + "," + newVendor_Categoty + ")";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", jsFunc, true);
                    return;
                }
                btn_find.Focus();
            }
        }
        #endregion

        #region Find Vendor category 99  populate the values  null in the right side
        public void SetEmptyControl(object sender, EventArgs e)
        {
            txt_NewVendorNo.Text = string.Empty;
            txt_NewVendorCat.Text = string.Empty;
            txt_NewVendor.Text = string.Empty;
            txt_newVendorCatDesc.Text = string.Empty;

            //Find text control set empty
            txt_fndNewVNo.Text = string.Empty;
            txt_fndNewCategory.Text = string.Empty;

            //Grid set null
            grdNewVendor.DataSource = null;
            grdNewVendor.DataBind();
        }
        #endregion
    }
}