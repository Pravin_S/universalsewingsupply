﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace uusitempricecost
{
    /// <summary>
    /// Summary description for UpdateVendors
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UpdateVendors : System.Web.Services.WebService
    {
        public UpdateVendors()
        {
            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }
        #region set,get properties
        public class VendorGrid
        {
            public string item_no { get; set; }
            public string Description { get; set; }
        }
        public class VendorDetails
        {
            public string VendorNo { get; set; }
            public string VendorCategory { get; set; }
            public string CompanyName { get; set; }
            public string Description { get; set; }
        }
        #endregion
        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public void save_Click(string VNos, string VNocats, string VDes)
        {
            var param = new DynamicParameters();
            param.Add("@VendorNo", VNos);
            param.Add("@VendorCategoryNo", VNocats);
            param.Add("@Description", VDes);

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DSN"].ConnectionString))
            {
                var mpresults = db.Execute("sp_update_vendorcategory_desc", param, commandType: CommandType.StoredProcedure);
            }
        }

        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public void LostFocusNewVCat(string fndNewVNo,string fndNewVCategory)
        {

        }


    }
}
