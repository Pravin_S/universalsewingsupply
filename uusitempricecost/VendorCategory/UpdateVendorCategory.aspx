﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="UpdateVendorCategory.aspx.cs" Inherits="uusitempricecost.VendorCategory.UpdateVendorCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/Content/Site1.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var popup;
        function createPopupWin(pageURL, pageTitle,
            popupWinWidth, popupWinHeight) {
            var left = (screen.width - popupWinWidth) / 2;
            var top = (screen.height - popupWinHeight) / 2;

            var myWindow = window.open(pageURL, pageTitle,
                'resizable=yes, width=' + popupWinWidth
                + ', height=' + popupWinHeight + ', top='
                + top + ', left=' + left);
        }
        function ShowPopup() {
            debugger;
            const params = new Proxy(new URLSearchParams(window.location.search), {
                get: (searchParams, prop) => searchParams.get(prop),
            });
            // Get the value of "some_key" in eg "https://example.com/?some_key=some_value"
            var VNo = params.VendorNo; // "some_value"
            var VCat = params.VendorCategory;

            popup = window.open(createPopupWin("Popup.aspx?cid=" + VNo + " &pwd=" + VCat + "",
                "", 480, 200));
            popup.focus();
            return false
        }
    </script>
        <div class="update-page-title">
            <div class="container">
                <h3>Update Vendor Categories</h3>
            </div>
        </div>
        <asp:Panel ID="updateCategory" runat="server">
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-lg-2 visible-lg-block"></div>
                            <div class="col-md-12 col-lg-4">
                                <asp:Label runat="server" ID="lblVendorNo" Text="Vendor Number"></asp:Label>
                                <asp:TextBox runat="server" ID="txtVendorNumber"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-lg-4">
                                <asp:Label runat="server" ID="lblVendorCategory" Text="Vendor Category"></asp:Label>
                                <asp:TextBox runat="server" ID="txtVendorCategory"></asp:TextBox>
                            </div>
                            <div class="col-lg-2 visible-lg-block"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-2">
                                <asp:Label runat="server" ID="lbl_Vendor" Text="Vendor"></asp:Label>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <asp:TextBox runat="server" ID="txt_VendorDesc"></asp:TextBox>
                            </div>
                            <div class="col-md-2 visible-lg-block"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-2">
                                <asp:Label runat="server" ID="lbl_CategoryDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <asp:TextBox runat="server" ID="txt_CategoryDesc"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-lg-2">
                                <asp:Button runat="server" Text="Update" ID="btnUpdateCat" CssClass="updatelbl" OnClientClick="javascript:ShowPopup();return false;" />
                            </div>
                        </div>
                    </div>
                    <%--New Vendor Desc--%>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-lg-2 visible-lg-block"></div>
                            <div class="col-md-12 col-lg-4">
                                <asp:Label runat="server" ID="lbl_NewVendor" Text="New Vendor Number"></asp:Label>
                                <asp:TextBox runat="server" ID="txt_NewVendorNo"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-lg-4">
                                <asp:Label runat="server" ID="lbl_NewVendorCategory" Text="New Vendor Category"></asp:Label>
                                <asp:TextBox runat="server" ID="txt_NewVendorCat"></asp:TextBox>
                            </div>
                            <div class="col-md-2 visible-lg-block"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-2">
                                <asp:Label runat="server" ID="lbl_NewVendors" Text="Vendor" CssClass="updatelblVen"></asp:Label>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <asp:TextBox runat="server" ID="txt_NewVendor"></asp:TextBox>
                            </div>
                            <div class="col-md-2 visible-lg-block"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-2">
                                <asp:Label runat="server" ID="lbl_newVendorCatDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <asp:TextBox runat="server" ID="txt_newVendorCatDesc"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-lg-2">
                                <asp:Button runat="server" Text="Update" ID="btn_newVendorCatDesc" CssClass="updatelbl" OnClientClick="javascript:ShowPopup();return false;" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </asp:Panel>

        <%--Min and max 100 button--%>
        <panel id="pnlVendorGrid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <asp:Button runat="server" ID="btn_min100" Text="-100" OnClick="btn_min100_Click" />
                        <asp:Button runat="server" ID="btn_max100" Text="+100" OnClick="btn_max100_Click" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div style="overflow-y: scroll; max-height: 300px;">
                            <asp:GridView runat="server" ID="grdVendor" AutoGenerateColumns="false" AllowPaging="true" PageSize="100" OnPageIndexChanging="grdVendor_PageIndexChanging"
                                 OnRowDataBound="grdVendor_RowDataBound" OnSelectedIndexChanged="grdVendor_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                </Columns>
                                <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
                            </asp:GridView>

                        </div>
                    </div>
                    <%--  Move to New Category--%>
                    <div class="col-md-2 updatevendorbtngroup">
                        <div class="movecategory">Move to New Category</div>
                        <asp:ImageButton runat="server" class="arrowbtn" ID="btn_singleArrow" OnClick="btn_singleArrow_Click" ImageUrl="~/Images/arrow_left_icon.svg" ImageAlign="Middle"/>
                        <asp:ImageButton runat="server" class="arrowbtn" ID="btn_singleLeftArrow" OnClick="btn_singleLeftArrow_Click" ImageUrl="~/Images/arrow_right_icon.svg" ImageAlign="Middle"/>
                        <asp:ImageButton runat="server" class="arrowbtn" ID="ImageButton1" OnClick="btn_singleArrow_Click" ImageUrl="~/Images/double_arrow_left_icon.svg" ImageAlign="Middle"/>
                        <asp:ImageButton runat="server" class="arrowbtn" ID="ImageButton2" OnClick="btn_singleLeftArrow_Click" ImageUrl="~/Images/double_arrow_right_icon.svg" ImageAlign="Middle"/>
                    </div>

                    <div class="col-md-4 updatelbl">
                        <div style="overflow-y: scroll; max-height: 300px;">
                            <asp:GridView runat="server" ID="grdNewVendor" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="grdNewVendor_PageIndexChanging" OnRowDataBound="grdNewVendor_RowDataBound" OnSelectedIndexChanged="grdNewVendor_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                </Columns>
                                <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
                            </asp:GridView>
                        </div>
                    </div>

                </div>
             </div>
        </panel>

    </div>
    
        <asp:Panel runat="server" ID="pnlFind" class="updatecontent">
            <div class="container">
                <div class="updatecontentfootbox">
                    <div class="row">
                        <div class="col-sm-6 col-md-5">
                            <div class="col-md-6 col-sm-12">
                                <asp:Label runat="server" ID="lblFndVNo"> Vendor Number</asp:Label>
                                <asp:TextBox runat="server" ID="txt_FndVNo" BackColor="Yellow" Width="110px" Height="40px"></asp:TextBox>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <asp:Label runat="server" ID="lbl_FndCategory"> Vendor Category</asp:Label>
                                <asp:TextBox runat="server" ID="txt_FndCategory" BackColor="Yellow" Width="110px" Height="40px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 visible-md-block visible-lg-block"></div>
                        <div class="col-sm-6 col-md-5">
                            <div class="col-md-6 col-sm-12">
                                <asp:Label runat="server" ID="lbl_fndNewVNo"> New Vendor Number</asp:Label>
                                <asp:TextBox runat="server" ID="txt_fndNewVNo" BackColor="Yellow" Width="110px" Height="40px"></asp:TextBox>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <asp:Label runat="server" ID="lbl_fndNewCategory"> New Vendor Category</asp:Label>
                                <asp:TextBox runat="server" ID="txt_fndNewCategory" BackColor="Yellow" Width="110px" Height="40px"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlButtons" CssClass="pnlbuttons">
            <div class="container">

                <asp:Button runat="server" ID="btn_find" Text="Find It" />

                <asp:Button runat="server" ID="btn_Reset" Text="Reset" OnClick="btn_Reset_Click"/>
                <br />
                <asp:Button runat="server" ID="btn_ViewVendorCat" Text="View Vendor Cat" OnClick="btn_ViewVendorCat_Click"/>

                <asp:Button runat="server" ID="btn_Exit" Text="Exit"/>
            </div>
        </asp:Panel>

    

</asp:Content>
