﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateVendorCategory.aspx.cs" Inherits="uusitempricecost.VendorCategory.UpdateVendorCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/Content/Site1.css" rel="stylesheet" />
    <link href="/Content/bootstrap.css" rel="stylesheet" />
    <div style="text-align: center">
        <h3>Update Vendor Categories</h3>
        <br />
        <br />
    </div>
    <div>
        <asp:Panel ID="updateCategory" runat="server">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lblVendorNo" Text="Vendor Number" Width="200px"></asp:Label>
                    <asp:TextBox runat="server" ID="txtVendorNumber"></asp:TextBox>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <asp:Label runat="server" ID="lblVendorCategory" Text="Vendor Category"></asp:Label>
                    <asp:TextBox runat="server" ID="txtVendorCategory"></asp:TextBox>
                </div>

                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                    <asp:Label runat="server" ID="lbl_NewVendor" Text="New Vendor Number"></asp:Label>
                    <asp:TextBox runat="server" ID="txt_NewVendorNo"></asp:TextBox>
                </div>

                <div class="col-md-2">
                    <asp:Label runat="server" ID="lbl_NewVendorCategory" Text="New Vendor Category"></asp:Label>
                    <asp:TextBox runat="server" ID="txt_NewVendorCat"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lbl_Vendor" Text="Vendor"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox runat="server" ID="txt_VendorDesc" Width="373px"></asp:TextBox>
                </div>
                <%--New Vendor Desc--%>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="lbl_NewVendors" Text="Vendor" CssClass="updatelblVen"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox runat="server" ID="txt_NewVendor" Width="373px"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lbl_CategoryDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox runat="server" ID="txt_CategoryDesc" Width="373px"></asp:TextBox>
                </div>
                <div class="col-md-3">
                    <asp:Button runat="server" Text="Update" ID="btnUpdateCat" CssClass="updatelbl" />
                </div>

                <%-- New Vendor --%>
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lbl_newVendorCatDesc" Text="Category Desc" CssClass="lblwrap"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox runat="server" ID="txt_newVendorCatDesc" Width="373px"></asp:TextBox>
                </div>
                <div class="col-md-1">
                    <asp:Button runat="server" Text="Update" ID="btn_newVendorCatDesc" CssClass="updatelbl" />
                </div>
            </div>
        </asp:Panel>


        <panel id="pnlVendorGrid">
            <div class="row">
                <div class="col-md-5">
                     <div style="overflow-y: scroll;height: 250px; width: 600px;">
                    <asp:GridView runat="server" ID="grdVendor" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="grdVendor_PageIndexChanging">
                        <Columns>
                            <asp:BoundField  DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"  />
                            <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"  />
                        </Columns>
                    </asp:GridView>
                        </div>
                     </div>
                     <div class="col-md-5 updatelbl">
                         <div style="overflow-y: scroll;height: 250px; width: 600px;">
                     <asp:GridView runat="server" ID="grdNewVendor" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="grdNewVendor_PageIndexChanging">
                        <Columns>
                            <asp:BoundField  DataField="item_no" HeaderText="Universal Part Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"/>
                            <asp:BoundField  DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                        </Columns>
                    </asp:GridView>
                             </div>
                         </div>
               
            </div>
        </panel>


    </div>
</asp:Content>
