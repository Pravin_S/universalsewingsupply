﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Vendor_Display.aspx.cs" Inherits="uusitempricecost.VendorCategory.Vendor_Display" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <link href="/Content/Site1.css" rel="stylesheet" />
        <asp:Panel runat="server" ID="pnlVendor">
            <%-- Gridview for vendor Display screen --%>
            <asp:GridView runat="server" ID="grdVendorDisplay" AutoGenerateColumns="false" AllowPaging="true" PageSize="30" OnPageIndexChanging="grdVendorDisplay_PageIndexChanging" OnRowCommand="grdVendorDisplay_RowCommand"
                Width="73%" HeaderStyle-BackColor="Yellow">
                <Columns>
                    <asp:BoundField ItemStyle-Width="100px" DataField="VendorName" HeaderText="Vendor Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-Height="30px" />

                    <asp:TemplateField HeaderText="Vendor Number" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:Label ID="lbl_VendorNumber" runat="server" Text='<%# Eval("VendorNumber") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField ItemStyle-Width="20px">
                        <HeaderTemplate>
                            <asp:Label ID="lbl_Defined" runat="server" Text="Defined Categories" ToolTip="Defined Categories" Width="50px"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_Define" runat="server" Text='<%# Bind("DefinedCategories") %>' ToolTip="Defined Categories" CssClass="lblDefined"></asp:Label>
                            <asp:ImageButton runat="server" Width="15" Height="15" ImageAlign="Right" ID="btn" ImageUrl="~/Images/arrow.png"
                                CommandName="Select" CommandArgument="<%# Container.DataItemIndex %>" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ItemStyle-Width="80px" DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200px" />
                </Columns>
                <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
            </asp:GridView>
            <%-- Custom Server Pagination --%>
            <asp:Repeater ID="rptPager" runat="server">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                        Enabled='<%# Eval("Enabled") %>' OnClick="lnkPage_Click"></asp:LinkButton>
                </ItemTemplate>
            </asp:Repeater>
            <div id="divBtn" class="editcontent">
                <asp:Panel runat="server" ID="editinputcontent">
                    <asp:Button runat="server" ID="btnVendorCat_Report" Text="Vendor Cat Report" OnClick="btnVendorCat_Report_Click" />
                    <asp:Button runat="server" ID="btnShow_All" Text="Show All Categories" OnClick="btnShow_All_Click" />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                   
                    <asp:Label runat="server" ID="lblVendor" Text="Vendor No.:" CssClass="lblVendor"></asp:Label>
                    <asp:TextBox runat="server" ID="txt_SearchVendor" Height="40px" Width="80px">
                    </asp:TextBox>

                    <asp:Button runat="server" Text="Find It" ID="btnFind" Height="40px" OnClick="btnFind_Click" />
                    <asp:Button runat="server" Text="Reset" ID="btnReset" Height="40px" OnClick="btnReset_Click" />
                    <asp:RegularExpressionValidator ID='vldNumber' ControlToValidate='txt_SearchVendor' Font-Bold="true" ForeColor="Red" Display='Dynamic' ErrorMessage='Enter numbers only' ValidationExpression='(^([0-9]*|\d*\d{1}?\d*)$)' runat='server'>
                    </asp:RegularExpressionValidator>
                </asp:Panel>

            </div>
        </asp:Panel>
    </div>


</asp:Content>
